# Pre-requisites:
# Install the required packages using below commands:
# `pip3 install pickle`
# `pip3 install flask`

from flask import Flask, request, render_template_string, make_response
import pickle
import base64

app = Flask(__name__)
app.secret_key = b'secret_key'
template_data = """
        <h1>%s</h1>
        <br/>
        <br/>
        <h2>Wizard Steps</h1>
        <p><a href="/step1">Step 1</a></p>
        <p><a href="/step2">Step 2</a></p>
        <p><a href="/step3">Step 3</a></p>
        <p><a href="/exploit">Exploit</a></p>
        <p><a href="/finish">Finish</a></p>
    """


class WizardData:
    def __init__(self, step1_data=None, step2_data=None, step3_data=None):
        self.step1_data = step1_data
        self.step2_data = step2_data
        self.step3_data = step3_data


def serialize_and_set_cookie(data_object):
    print("in serialize")
    serialized_data = pickle.dumps(data_object)
    response = make_response()
    response.set_cookie('wizard_data', base64.b64encode(serialized_data).decode('utf-8'))
    return response


def get_and_deserialize_cookie():
    serialized_data = request.cookies.get('wizard_data', None)
    if serialized_data:
        print("in deserialize")
        # ruleid: python_deserialization_rule-pickle
        return pickle.loads(base64.b64decode(serialized_data))
    return WizardData()


@app.route('/')
def index():
    return render_template_string(template_data % "")


@app.route('/step1')
def step1():
    wizard_data = get_and_deserialize_cookie()
    wizard_data.step1_data = "Step 1 Data"
    response = serialize_and_set_cookie(wizard_data)
    response.data = template_data % "Step 1 Done"
    return response


@app.route('/step2')
def step2():
    wizard_data = get_and_deserialize_cookie()
    wizard_data.step2_data = "Step 2 Data"
    response = serialize_and_set_cookie(wizard_data)
    response.data = template_data % "Step 2 Done"
    return response


@app.route('/step3')
def step3():
    wizard_data = get_and_deserialize_cookie()
    wizard_data.step3_data = "Step 3 Data"
    response = serialize_and_set_cookie(wizard_data)
    response.data = template_data % "Step 3 Done"
    return response


@app.route('/finish')
def finish():
    wizard_data = get_and_deserialize_cookie()
    return template_data % f"Wizard Completed. Data: {wizard_data.step1_data}, {wizard_data.step2_data}, {wizard_data.step3_data}"


# For the purpose of demonstration of the vulnerability this is conveniently made available on the server side.
# At any point the end-user can make a request to this route to add malicious data "PickleRCEExploit" to the cookie and
# the next time it is deserialized, it will execute the command in that object.
@app.route('/exploit')
def exploit():
    wizard_data = get_and_deserialize_cookie()
    response = serialize_and_set_cookie(
        PickleRCEExploit(wizard_data.step1_data, wizard_data.step2_data, wizard_data.step3_data)
    )
    response.data = template_data % "Exploit set in cookie."
    return response


class PickleRCEExploit(WizardData):
    def __init__(self, step1_data=None, step2_data=None, step3_data=None):
        super().__init__(step1_data, step2_data, step3_data)

    def __reduce__(self):
        import subprocess
        command = "ls"  # replace with your command
        self.step1_data = subprocess.getoutput(command)
        return self.__class__, (self.step1_data, self.step2_data, self.step3_data)


if __name__ == '__main__':
    app.run(debug=True)
